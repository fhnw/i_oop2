package ch.fhnw.DieterHolz_JavaFX_Examples.UndoRedo.undoredoaddremove.presentationmodels;

/**
 * @author Dieter Holz
 */
public class AddCommand implements Command {

	private final EuropePM  europe;
	private final CountryPM added;
	private final int       position;

	public AddCommand(EuropePM europe, CountryPM added, int position) {
		this.europe = europe;
		this.added = added;
		this.position = position;
	}

	@Override
	public void undo() {
		europe.removeFromList(added);
	}

	@Override
	public void redo() {
		europe.addToList(position, added);
	}
}
