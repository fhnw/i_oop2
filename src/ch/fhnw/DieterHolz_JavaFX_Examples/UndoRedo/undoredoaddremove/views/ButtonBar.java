package ch.fhnw.DieterHolz_JavaFX_Examples.UndoRedo.undoredoaddremove.views;

import ch.fhnw.DieterHolz_JavaFX_Examples.UndoRedo.undoredoaddremove.presentationmodels.EuropePM;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

/**
 * @author Dieter Holz
 */
public class ButtonBar extends HBox implements ViewMixin {

	private final EuropePM europe;

	private Button undoButton;
	private Button redoButton;
	private Button addButton;
	private Button removeButton;

	public ButtonBar(EuropePM europe) {
		this.europe = europe;
		getStyleClass().add("buttonbar");
		init();
	}

	@Override
	public void initializeControls() {
		undoButton = new Button("Undo");
		redoButton = new Button("Redo");
		addButton = new Button("Add");
		removeButton = new Button("Remove");
	}

	@Override
	public void layoutControls() {
		HBox spacer = new HBox();
		HBox.setHgrow(spacer, Priority.ALWAYS);

		getChildren().addAll(addButton, removeButton, spacer, undoButton, redoButton);
	}

	@Override
	public void addEventHandlers() {
		undoButton.setOnAction(event -> europe.undo());
		redoButton.setOnAction(event -> europe.redo());

		addButton.setOnAction(event -> europe.addNewCountry());
		removeButton.setOnAction(event -> europe.removeCountry());
	}

	@Override
	public void addBindings() {
		undoButton.disableProperty().bind(europe.undoDisabledProperty());
		redoButton.disableProperty().bind(europe.redoDisabledProperty());
	}
}
