package ch.fhnw.DieterHolz_JavaFX_Examples.UndoRedo.undoredoaddremove.views;

import ch.fhnw.DieterHolz_JavaFX_Examples.UndoRedo.undoredoaddremove.presentationmodels.EuropePM;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

public class ApplicationUI extends BorderPane implements ViewMixin {
	private final EuropePM model;

	private CountryHeader countryHeader;
	private CountryForm   countryForm;
	private SelectorBar   toolbar;
	private ButtonBar     buttonBar;

	public ApplicationUI(EuropePM model) {
		this.model = model;
		init();
	}

	@Override
	public void initializeControls() {
		countryHeader = new CountryHeader(model);
		countryForm = new CountryForm(model);
		toolbar = new SelectorBar(model);
		buttonBar = new ButtonBar(model);
	}

	@Override
	public void layoutControls() {
		setTop(toolbar);
		setCenter(new VBox(countryHeader, countryForm));
		setBottom(buttonBar);
	}

}
