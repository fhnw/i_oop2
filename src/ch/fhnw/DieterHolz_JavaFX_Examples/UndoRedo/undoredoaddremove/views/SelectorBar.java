package ch.fhnw.DieterHolz_JavaFX_Examples.UndoRedo.undoredoaddremove.views;

import ch.fhnw.DieterHolz_JavaFX_Examples.UndoRedo.undoredoaddremove.presentationmodels.EuropePM;
import javafx.beans.binding.Bindings;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.util.converter.NumberStringConverter;

/**
 * @author Dieter Holz
 */
public class SelectorBar extends HBox implements ViewMixin {

	private final EuropePM europe;

	private Slider    slider;
	private TextField inputField;

	public SelectorBar(EuropePM europe) {
		super();
		this.europe = europe;
		getStyleClass().add("selectorbar");
		init();
	}

	@Override
	public void initializeControls() {
		slider = new Slider();
		slider.setMin(0.0);
		inputField = new TextField();
	}

	@Override
	public void layoutControls() {
		getChildren().addAll(slider, inputField);
	}

	@Override
	public void addBindings() {
		slider.maxProperty().bind(Bindings.size(europe.allCountries()).subtract(1));

		//slider.valueProperty().bindBidirectional(europe.selectedCountryIdProperty());
		slider.valueProperty().bindBidirectional(europe.selectedIndexProperty());


		Bindings.bindBidirectional(inputField.textProperty(), europe.selectedCountryIdProperty(), new NumberStringConverter());
	}
}
