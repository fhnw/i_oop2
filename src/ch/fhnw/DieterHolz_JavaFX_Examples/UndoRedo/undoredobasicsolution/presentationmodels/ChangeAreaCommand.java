package ch.fhnw.DieterHolz_JavaFX_Examples.UndoRedo.undoredobasicsolution.presentationmodels;

/**
 * @author Dieter Holz
 */
public class ChangeAreaCommand implements Command {

    private final EuropePM  europe;
    private final CountryPM country;
    private final double    newValue;
    private final double    oldValue;

    public ChangeAreaCommand(EuropePM europe, CountryPM countryPM, double oldValue, double newValue) {
        this.europe = europe;
        this.country = countryPM;
        this.oldValue = oldValue;
        this.newValue = newValue;
    }

    @Override
    public void undo() {
        europe.changeArea(country, oldValue);
    }

    @Override
    public void redo() {
        europe.changeArea(country, newValue);
    }
}
