package ch.fhnw.DieterHolz_JavaFX_Examples.UndoRedo.undoredobasicsolution.presentationmodels;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * @author Dieter Holz
 */
public class EuropePM {
	private final StringProperty  applicationTitle  = new SimpleStringProperty("Selection Handling");
	private final IntegerProperty selectedCountryId = new SimpleIntegerProperty(-1);

	private final ObservableList<Command> undoStack = FXCollections.observableArrayList();
	private final ObservableList<Command> redoStack = FXCollections.observableArrayList();

	private final BooleanProperty undoDisabled = new SimpleBooleanProperty();
	private final BooleanProperty redoDisabled = new SimpleBooleanProperty();

	private ObservableList<CountryPM> allCountries = FXCollections.observableArrayList();

	private final CountryPM countryProxy = new CountryPM();
	private ChangeListener<String> changeNameListener ;
	private ChangeListener<Number> changeAreaListener;

	public EuropePM() {
		this(getAllCountries());
	}

	public EuropePM(CountryPM... countries) {
		this(new ArrayList(Arrays.asList(countries)));
	}

	public EuropePM(List<CountryPM> countryList) {
		allCountries.addAll(countryList);

		// das Binding machen wir hier und NICHT im UI. Dann können alle UI-Teilbereiche diese Grundfunktionalität nutzen
		// und das Enabling/Disabling ist mit Unit-Tests testbar.
		undoDisabled.bind(Bindings.isEmpty(undoStack));
		redoDisabled.bind(Bindings.isEmpty(redoStack));

		selectedCountryIdProperty().addListener((observable, oldValue, newValue) -> {
			                                        CountryPM oldSelection = getCountry((int) oldValue);
			                                        CountryPM newSelection = getCountry((int) newValue);

			                                        if (oldSelection != null) {
				                                        countryProxy.idProperty().unbindBidirectional(oldSelection.idProperty());
				                                        countryProxy.nameProperty().unbindBidirectional(oldSelection.nameProperty());
				                                        countryProxy.areaProperty().unbindBidirectional(oldSelection.areaProperty());

				                                        // nicht selektierte Countries können auch nicht verändert werden
				                                        // die ChangeListener kann man wieder entfernen
				                                        oldSelection.nameProperty().removeListener(changeNameListener);
				                                        oldSelection.areaProperty().removeListener(changeAreaListener);
			                                        }

			                                        if (newSelection != null) {
				                                        countryProxy.idProperty().bindBidirectional(newSelection.idProperty());
				                                        countryProxy.nameProperty().bindBidirectional(newSelection.nameProperty());
				                                        countryProxy.areaProperty().bindBidirectional(newSelection.areaProperty());

				                                        // bei jeder Aenderung der name-Property muss eine neue Command-Instanz abgespeichert werden.
				                                        changeNameListener = (property, oldName, newName) -> {
					                                        redoStack.clear();  // sobald eine Veränderung vorgenommen wird, ist soll kein Redo mehr möglich sein
					                                        undoStack.add(0, new ChangeNameCommand(EuropePM.this, newSelection, oldName, newName));
				                                        };

				                                        newSelection.nameProperty().addListener(changeNameListener);

				                                        // bei jeder Aenderung der area-Property muss eine neue Command-Instanz abgespeichert werden.
				                                        changeAreaListener = (property, oldArea, newArea) -> {
					                                        redoStack.clear();
					                                        undoStack.add(0, new ChangeAreaCommand(EuropePM.this, newSelection, oldArea.doubleValue(), newArea.doubleValue()));
				                                        };

				                                        newSelection.areaProperty().addListener(changeAreaListener);
			                                        }
		                                        }
		                                       );
	}

	public void undo() {
		if (undoStack.isEmpty()) {
			return;
		}

		Command lastCommand = undoStack.get(0);
		undoStack.remove(0);

		// abspeichern auf dem RedoStack damit redo ausgeführt werden kann
		redoStack.add(0, lastCommand);

		lastCommand.undo();
	}

	public void redo() {
		if (redoStack.isEmpty()) {
			return;
		}

		Command lastCommand = redoStack.get(0);
		redoStack.remove(0);

		//abspeichern auf dem Undo-Stack, damit das Redo wieder zurückgenommen werden kann
		undoStack.add(0, lastCommand);

		lastCommand.redo();
	}

	public void changeName(CountryPM country, String value) {
		// zuerst den Listener entfernen, damit nicht sofort wieder ein Command-Objekt erzeugt wird
		country.nameProperty().removeListener(changeNameListener);

		// setzen des Names ohne weiteres Command-Objekt
		country.setName(value);

		// dann den ChangeListener wieder aktivieren
		country.nameProperty().addListener(changeNameListener);
	}

	public void changeArea(CountryPM country, double area) {
		country.areaProperty().removeListener(changeAreaListener);
		country.setArea(area);
		country.areaProperty().addListener(changeAreaListener);
	}

	public final CountryPM getCountryProxy() {
		return countryProxy;
	}

	private CountryPM getCountry(int id) {
		Optional<CountryPM> pmOptional = allCountries.stream()
		                                             .filter(countryPM -> countryPM.getId() == id)
		                                             .findAny();
		return pmOptional.isPresent() ? pmOptional.get() : null;
	}

	private static List<CountryPM> getAllCountries() {
		return Arrays.asList(new CountryPM(0, "Schweiz", 41_285),
		                     new CountryPM(1, "Deutschland", 357_121.41),
		                     new CountryPM(2, "Frankreich", 668_763.00),
		                     new CountryPM(3, "Italien", 301_338),
		                     new CountryPM(4, "Oesterreich", 83_878.99));
	}

	public ObservableList<CountryPM> allCountries() {
		return allCountries;
	}

	public String getApplicationTitle() {
		return applicationTitle.get();
	}

	public StringProperty applicationTitleProperty() {
		return applicationTitle;
	}

	public void setApplicationTitle(String applicationTitle) {
		this.applicationTitle.set(applicationTitle);
	}

	public int getSelectedCountryId() {
		return selectedCountryId.get();
	}

	public IntegerProperty selectedCountryIdProperty() {
		return selectedCountryId;
	}

	public void setSelectedCountryId(int selectedCountryId) {
		this.selectedCountryId.set(selectedCountryId);
	}

	public boolean getUndoDisabled() {
		return undoDisabled.get();
	}

	public BooleanProperty undoDisabledProperty() {
		return undoDisabled;
	}

	public void setUndoDisabled(boolean undoDisabled) {
		this.undoDisabled.set(undoDisabled);
	}

	public boolean getRedoDisabled() {
		return redoDisabled.get();
	}

	public BooleanProperty redoDisabledProperty() {
		return redoDisabled;
	}

	public void setRedoDisabled(boolean redoDisabled) {
		this.redoDisabled.set(redoDisabled);
	}

}
