package ch.fhnw.DieterHolz_JavaFX_Examples.UndoRedo.undoredobasicsolution.presentationmodels;

/**
 * @author Dieter Holz
 */
public class ChangeNameCommand implements Command {

    private final EuropePM  europe;
    private final CountryPM country;
    private final String    newValue;
    private final String    oldValue;

    public ChangeNameCommand(EuropePM europe, CountryPM countryPM, String oldValue, String newValue) {
        this.europe = europe;
        this.country = countryPM;
        this.oldValue = oldValue;
        this.newValue = newValue;
    }

    @Override
    public void undo() {
        europe.changeName(country, oldValue);
    }

    @Override
    public void redo() {
        europe.changeName(country, newValue);
    }
}
