package ch.fhnw.DieterHolz_JavaFX_Examples.UndoRedo.undoredobasicsolution.views;

import ch.fhnw.DieterHolz_JavaFX_Examples.UndoRedo.undoredobasicsolution.presentationmodels.CountryPM;
import ch.fhnw.DieterHolz_JavaFX_Examples.UndoRedo.undoredobasicsolution.presentationmodels.EuropePM;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

/**
 * @author Dieter Holz
 */
public class CountryHeader extends VBox implements ViewMixin {

	private final EuropePM europe;

	private Label nameLabel;
	private Label areaLabel;

	public CountryHeader(EuropePM europe) {
		this.europe = europe;
		getStyleClass().add("header");
		init();
	}

	@Override
	public void initializeControls() {
		nameLabel = new Label();
		areaLabel = new Label();
	}

	@Override
	public void layoutControls() {
		getChildren().addAll(nameLabel, areaLabel);
	}

	@Override
	public void addBindings() {
		CountryPM proxy = europe.getCountryProxy();

		nameLabel.textProperty().bind(proxy.nameProperty());
		areaLabel.textProperty().bind(proxy.areaProperty().asString("%.2f km2"));
	}
}
