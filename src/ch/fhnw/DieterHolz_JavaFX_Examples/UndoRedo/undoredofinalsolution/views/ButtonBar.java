package ch.fhnw.DieterHolz_JavaFX_Examples.UndoRedo.undoredofinalsolution.views;

import ch.fhnw.DieterHolz_JavaFX_Examples.UndoRedo.undoredofinalsolution.presentationmodels.EuropePM;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;

/**
 * @author Dieter Holz
 */
public class ButtonBar extends HBox implements ViewMixin {

	private final EuropePM europe;

	private Button undoButton;
	private Button redoButton;

	public ButtonBar(EuropePM europe) {
		this.europe = europe;
		getStyleClass().add("buttonbar");
		init();
	}

	@Override
	public void initializeControls() {
		undoButton = new Button("Undo");
		redoButton = new Button("Redo");
	}

	@Override
	public void layoutControls() {
		getChildren().addAll(undoButton, redoButton);
	}

	@Override
	public void addEventHandlers() {
		undoButton.setOnAction(event -> europe.undo());
		redoButton.setOnAction(event -> europe.redo());
	}

	@Override
	public void addBindings() {
		undoButton.disableProperty().bind(europe.undoDisabledProperty());
		redoButton.disableProperty().bind(europe.redoDisabledProperty());
	}
}
