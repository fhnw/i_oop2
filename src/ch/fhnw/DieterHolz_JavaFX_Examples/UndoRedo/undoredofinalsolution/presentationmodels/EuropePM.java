package ch.fhnw.DieterHolz_JavaFX_Examples.UndoRedo.undoredofinalsolution.presentationmodels;

import java.util.Arrays;
import java.util.List;

import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * @author Dieter Holz
 */
public class EuropePM {
	private final ObservableList<Command> undoStack = FXCollections.observableArrayList();
	private final ObservableList<Command> redoStack = FXCollections.observableArrayList();

	private final StringProperty  applicationTitle  = new SimpleStringProperty("Selection Handling");
	private final IntegerProperty selectedCountryId = new SimpleIntegerProperty(-1);

	private final BooleanProperty undoDisabled = new SimpleBooleanProperty();
	private final BooleanProperty redoDisabled = new SimpleBooleanProperty();

	private ObservableList<CountryPM> allCountries = FXCollections.observableArrayList();

	private final CountryPM countryProxy = new CountryPM();

	private final ChangeListener propertyChangeListenerForUndoSupport = (observable, oldValue, newValue) -> {
		redoStack.clear();
		undoStack.add(0, new ValueChangeCommand(EuropePM.this, (Property) observable, oldValue, newValue));
	};

	public EuropePM() {
		this(getAllCountries());
	}

	public EuropePM(List<CountryPM> countryList) {
		allCountries.addAll(countryList);

		undoDisabled.bind(Bindings.isEmpty(undoStack));
		redoDisabled.bind(Bindings.isEmpty(redoStack));

		selectedCountryIdProperty().addListener((observable, oldValue, newValue) -> {
			                                        CountryPM oldSelection = getCountry((int) oldValue);
			                                        CountryPM newSelection = getCountry((int) newValue);

			                                        if (oldSelection != null) {
				                                        unbindFromProxy(oldSelection);
				                                        disableUndoSupport(oldSelection);
			                                        }

			                                        if (newSelection != null) {
				                                        bindToProxy(newSelection);
				                                        enableUndoSupport(newSelection);
			                                        }
		                                        }
		                                       );

		setSelectedCountryId(0);

		// selection changes are undoable
		selectedCountryIdProperty().addListener(propertyChangeListenerForUndoSupport);
	}

	public CountryPM getCountryProxy() {
		return countryProxy;
	}

	public ObservableList<CountryPM> allCountries() {
		return allCountries;
	}

	public <T> void setPropertyValueWithoutUndoSupport(Property<T> property, T newValue){
		property.removeListener(propertyChangeListenerForUndoSupport);
		property.setValue(newValue);
		property.addListener(propertyChangeListenerForUndoSupport);
	}

	public void undo() {
		if (undoStack.isEmpty()) {
			return;
		}
		Command cmd = undoStack.get(0);
		undoStack.remove(0);
		redoStack.add(0, cmd);

		cmd.undo();
	}

	public void redo() {
		if (redoStack.isEmpty()) {
			return;
		}
		Command cmd = redoStack.get(0);
		redoStack.remove(0);
		undoStack.add(0, cmd);

		cmd.redo();
	}

	private void disableUndoSupport(CountryPM country) {
		country.idProperty().removeListener(propertyChangeListenerForUndoSupport);
		country.nameProperty().removeListener(propertyChangeListenerForUndoSupport);
		country.areaProperty().removeListener(propertyChangeListenerForUndoSupport);
	}

	private void enableUndoSupport(CountryPM country) {
		country.idProperty().addListener(propertyChangeListenerForUndoSupport);
		country.nameProperty().addListener(propertyChangeListenerForUndoSupport);
		country.areaProperty().addListener(propertyChangeListenerForUndoSupport);
	}

	private void bindToProxy(CountryPM country) {
		countryProxy.idProperty().bindBidirectional(country.idProperty());
		countryProxy.nameProperty().bindBidirectional(country.nameProperty());
		countryProxy.areaProperty().bindBidirectional(country.areaProperty());
	}

	private void unbindFromProxy(CountryPM country) {
		countryProxy.idProperty().unbindBidirectional(country.idProperty());
		countryProxy.nameProperty().unbindBidirectional(country.nameProperty());
		countryProxy.areaProperty().unbindBidirectional(country.areaProperty());
	}

	private CountryPM getCountry(int id) {
        return allCountries.stream()
                           .filter(countryPM -> countryPM.getId() == id)
                           .findAny().orElse(null);
    }

	private static List<CountryPM> getAllCountries() {
		return Arrays.asList(new CountryPM(0, "Schweiz", 41_285),
		                     new CountryPM(1, "Deutschland", 357_121.41),
		                     new CountryPM(2, "Frankreich", 668_763.00),
		                     new CountryPM(3, "Italien", 301_338),
		                     new CountryPM(4, "Oesterreich", 83_878.99));
	}

	public String getApplicationTitle() {
		return applicationTitle.get();
	}

	public StringProperty applicationTitleProperty() {
		return applicationTitle;
	}

	public void setApplicationTitle(String applicationTitle) {
		this.applicationTitle.set(applicationTitle);
	}

	public int getSelectedCountryId() {
		return selectedCountryId.get();
	}

	public IntegerProperty selectedCountryIdProperty() {
		return selectedCountryId;
	}

	public void setSelectedCountryId(int selectedCountryId) {
		this.selectedCountryId.set(selectedCountryId);
	}

	public boolean getUndoDisabled() {
		return undoDisabled.get();
	}

	public BooleanProperty undoDisabledProperty() {
		return undoDisabled;
	}

	public void setUndoDisabled(boolean undoDisabled) {
		this.undoDisabled.set(undoDisabled);
	}

	public boolean getRedoDisabled() {
		return redoDisabled.get();
	}

	public BooleanProperty redoDisabledProperty() {
		return redoDisabled;
	}

	public void setRedoDisabled(boolean redoDisabled) {
		this.redoDisabled.set(redoDisabled);
	}
}
