package ch.fhnw.DieterHolz_JavaFX_Examples.UndoRedo.undoredofinalsolution.presentationmodels;

import javafx.beans.property.Property;

/**
 * @author Dieter Holz
 */
public class ValueChangeCommand<T> implements Command {
	private final EuropePM    europe;
	private final Property<T> property;
	private final T           oldValue;
	private final T           newValue;

	public ValueChangeCommand(EuropePM europe, Property<T> property, T oldValue, T newValue) {
		this.europe = europe;
		this.property = property;
		this.oldValue = oldValue;
		this.newValue = newValue;
	}

	public void undo() {
		europe.setPropertyValueWithoutUndoSupport(property, oldValue);
	}

	public void redo() {
		europe.setPropertyValueWithoutUndoSupport(property, newValue);
	}
}
