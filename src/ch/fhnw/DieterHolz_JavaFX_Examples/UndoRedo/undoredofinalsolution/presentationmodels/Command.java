package ch.fhnw.DieterHolz_JavaFX_Examples.UndoRedo.undoredofinalsolution.presentationmodels;

/**
 * @author Dieter Holz
 */
public interface Command {
	void undo();

	void redo();
}
