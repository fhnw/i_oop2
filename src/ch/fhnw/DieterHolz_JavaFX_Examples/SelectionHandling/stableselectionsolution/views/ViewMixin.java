package ch.fhnw.DieterHolz_JavaFX_Examples.SelectionHandling.stableselectionsolution.views;

/**
 * @author Dieter Holz
 */
public interface ViewMixin {

	default void init() {
		initializeControls();
		layoutControls();
		addEventHandlers();
		addValueChangedListeners();
		addBindings();
	}

	void initializeControls();

	void layoutControls();

	default void addEventHandlers() {
	}

	default void addValueChangedListeners() {
	}

	default void addBindings() {
	}
}
