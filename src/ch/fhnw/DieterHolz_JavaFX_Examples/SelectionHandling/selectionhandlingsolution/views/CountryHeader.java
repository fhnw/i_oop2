package ch.fhnw.DieterHolz_JavaFX_Examples.SelectionHandling.selectionhandlingsolution.views;

import ch.fhnw.DieterHolz_JavaFX_Examples.SelectionHandling.selectionhandlingsolution.presentationmodels.CountryPM;
import ch.fhnw.DieterHolz_JavaFX_Examples.SelectionHandling.selectionhandlingsolution.presentationmodels.EuropePM;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

/**
 * @author Dieter Holz
 */
public class CountryHeader extends VBox implements ViewMixin {

	private final EuropePM europe;

	private Label nameLabel;
	private Label areaLabel;

	public CountryHeader(EuropePM europe) {
		this.europe = europe;
		getStyleClass().add("header");
		init();
	}

	@Override
	public void initializeControls() {
		nameLabel = new Label();
		areaLabel = new Label();
	}

	@Override
	public void layoutControls() {
		getChildren().addAll(nameLabel, areaLabel);
	}

	@Override
	public void addValueChangedListeners() {
		europe.selectedCountryIdProperty().addListener((observable, oldValue, newValue) -> {
			CountryPM oldSelection = europe.getCountry((int) oldValue);
			CountryPM newSelection = europe.getCountry((int) newValue);

			if (oldSelection != null) {
				nameLabel.textProperty().unbind();
				areaLabel.textProperty().unbind();
			}

			if (newSelection != null) {
				nameLabel.textProperty().bind(newSelection.nameProperty());
				areaLabel.textProperty().bind(newSelection.areaProperty().asString("%.2f km2"));
			}
		});
	}
}
