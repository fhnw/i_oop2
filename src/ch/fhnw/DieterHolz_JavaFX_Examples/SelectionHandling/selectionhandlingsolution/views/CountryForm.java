package ch.fhnw.DieterHolz_JavaFX_Examples.SelectionHandling.selectionhandlingsolution.views;

import java.util.Locale;

import ch.fhnw.DieterHolz_JavaFX_Examples.SelectionHandling.selectionhandlingsolution.presentationmodels.CountryPM;
import ch.fhnw.DieterHolz_JavaFX_Examples.SelectionHandling.selectionhandlingsolution.presentationmodels.EuropePM;
import javafx.beans.binding.Bindings;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.util.converter.NumberStringConverter;

/**
 * @author Dieter Holz
 */
public class CountryForm extends GridPane implements ViewMixin {

	private final EuropePM europe;

	private Label     idLabel;
	private Label     idField;
	private Label     nameLabel;
	private TextField nameField;
	private Label     areaLabel;
	private TextField areaField;

	public CountryForm(EuropePM europe) {
		this.europe = europe;
		getStyleClass().add("form");
		init();
	}

	@Override
	public void initializeControls() {
		idLabel = new Label("Id");
		idField = new Label();

		nameLabel = new Label("Name");
		nameField = new TextField();

		areaLabel = new Label("Fläche in km2");
		areaField = new TextField();
	}

	@Override
	public void layoutControls() {
		ColumnConstraints grow = new ColumnConstraints();
		grow.setHgrow(Priority.ALWAYS);
		getColumnConstraints().addAll(new ColumnConstraints(), grow);

		addRow(0, idLabel, idField);
		addRow(1, nameLabel, nameField);
		addRow(2, areaLabel, areaField);
	}

	@Override
	public void addValueChangedListeners() {
		europe.selectedCountryIdProperty().addListener((observable, oldValue, newValue) -> {
			CountryPM oldSelection = europe.getCountry(oldValue.intValue());
			CountryPM newSelection = europe.getCountry(newValue.intValue());

			if (oldSelection != null) {
				idField.textProperty().unbind();
				nameField.textProperty().unbindBidirectional(oldSelection.nameProperty());
				areaField.textProperty().unbindBidirectional(oldSelection.areaProperty());
			}

			if (newSelection != null) {
				idField.textProperty().bind(newSelection.idProperty().asString());
				nameField.textProperty().bindBidirectional(newSelection.nameProperty());
				Bindings.bindBidirectional(areaField.textProperty(), newSelection.areaProperty(), new NumberStringConverter(new Locale("de", "CH")));
			}
		});
	}
}
