package ch.fhnw.DieterHolz_JavaFX_Examples.SelectionHandling.stableselection.views;

import ch.fhnw.DieterHolz_JavaFX_Examples.SelectionHandling.stableselection.presentationmodels.EuropePM;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

/**
 * @author Dieter Holz
 */
public class CountryHeader extends VBox implements ViewMixin {

	private final EuropePM europe;

	private Label nameLabel;
	private Label areaLabel;

	public CountryHeader(EuropePM europe) {
		this.europe = europe;
		getStyleClass().add("header");
		init();
	}

	@Override
	public void initializeControls() {
		nameLabel = new Label();
		areaLabel = new Label();
	}

	@Override
	public void layoutControls() {
		getChildren().addAll(nameLabel, areaLabel);
	}

}
