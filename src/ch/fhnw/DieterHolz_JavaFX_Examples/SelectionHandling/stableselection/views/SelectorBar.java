package ch.fhnw.DieterHolz_JavaFX_Examples.SelectionHandling.stableselection.views;

import ch.fhnw.DieterHolz_JavaFX_Examples.SelectionHandling.stableselection.presentationmodels.CountryPM;
import ch.fhnw.DieterHolz_JavaFX_Examples.SelectionHandling.stableselection.presentationmodels.EuropePM;
import javafx.beans.binding.Bindings;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.util.converter.NumberStringConverter;

/**
 * @author Dieter Holz
 */
public class SelectorBar extends HBox implements ViewMixin {

	private final EuropePM europe;

	private Slider              slider;
	private TextField           inputField;
	private ComboBox<CountryPM> comboBox;

	public SelectorBar(EuropePM europe) {
		super();
		this.europe = europe;
		getStyleClass().add("selectorbar");
		init();
	}

	@Override
	public void initializeControls() {
		slider = new Slider();
		slider.setMin(0.0);
		slider.setMax(europe.allCountries().size() - 1);
		inputField = new TextField();
		comboBox = new ComboBox<>(europe.allCountries());
	}

	@Override
	public void layoutControls() {
		getChildren().addAll(slider, inputField, comboBox);
	}

	@Override
	public void addBindings() {
		slider.valueProperty().bindBidirectional(europe.selectedCountryIdProperty());
		Bindings.bindBidirectional(inputField.textProperty(), europe.selectedCountryIdProperty(), new NumberStringConverter());
	}

	@Override
	public void addValueChangedListeners() {
		comboBox.valueProperty().addListener((observable, oldValue, newValue) -> {
			europe.setSelectedCountryId(newValue.getId());
		});

		europe.selectedCountryIdProperty().addListener((observable, oldValue, newValue) -> {
			comboBox.getSelectionModel().select((int) newValue);
		});
	}
}
