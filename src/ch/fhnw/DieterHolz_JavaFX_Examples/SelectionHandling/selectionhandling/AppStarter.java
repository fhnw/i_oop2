package ch.fhnw.DieterHolz_JavaFX_Examples.SelectionHandling.selectionhandling;

import ch.fhnw.DieterHolz_JavaFX_Examples.SelectionHandling.selectionhandling.presentationmodels.EuropePM;
import ch.fhnw.DieterHolz_JavaFX_Examples.SelectionHandling.selectionhandling.views.ApplicationUI;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class AppStarter extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		EuropePM model = new EuropePM();

		Parent rootPanel = new ApplicationUI(model);

		Scene scene = new Scene(rootPanel);

		String stylesheet = getClass().getResource("style.css").toExternalForm();
		scene.getStylesheets().add(stylesheet);

		primaryStage.titleProperty().bind(model.applicationTitleProperty());
		primaryStage.setScene(scene);

		primaryStage.setResizable(false);

		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
