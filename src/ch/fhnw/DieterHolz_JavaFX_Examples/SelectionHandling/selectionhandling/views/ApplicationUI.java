package ch.fhnw.DieterHolz_JavaFX_Examples.SelectionHandling.selectionhandling.views;

import ch.fhnw.DieterHolz_JavaFX_Examples.SelectionHandling.selectionhandling.presentationmodels.EuropePM;
import javafx.scene.layout.BorderPane;

public class ApplicationUI extends BorderPane implements ViewMixin {
	private final EuropePM model;

	private CountryForm countryForm;
	private SelectorBar toolbar;

	public ApplicationUI(EuropePM model) {
		this.model = model;
		init();
	}

	@Override
	public void initializeControls() {
		countryForm = new CountryForm(model);
		toolbar = new SelectorBar(model);
	}

	@Override
	public void layoutControls() {
		setTop(toolbar);
		setCenter(countryForm);
	}

}
