package ch.fhnw.DieterHolz_JavaFX_Examples.SelectionHandling.selectionhandling.views;

import ch.fhnw.DieterHolz_JavaFX_Examples.SelectionHandling.selectionhandling.presentationmodels.EuropePM;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

/**
 * @author Dieter Holz
 */
public class SelectorBar extends HBox implements ViewMixin {

	private final EuropePM europe;

	private Slider    slider;
	private TextField inputField;

	public SelectorBar(EuropePM europe) {
		this.europe = europe;
		getStyleClass().add("selectorbar");
		init();
	}


	@Override
	public void initializeControls() {
		slider = new Slider();
		slider.setMin(0.0);
		slider.setMax(europe.getAllCountries().size() - 1);
		inputField = new TextField();
	}

	@Override
	public void layoutControls() {
		getChildren().addAll(slider, inputField);
	}

}
