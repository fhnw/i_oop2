package ch.fhnw.DieterHolz_JavaFX_Examples.SelectionHandling.selectionhandling.presentationmodels;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * @author Dieter Holz
 */
public class EuropePM {
	private final StringProperty applicationTitle = new SimpleStringProperty("Selection Handling");

	private ObservableList<CountryPM> allCountries = FXCollections.observableArrayList();

	public EuropePM() {
		allCountries.addAll(createCountryList());
	}

	private static List<CountryPM> createCountryList() {
		return Arrays.asList(new CountryPM(0, "Schweiz",      41_285.00),
		                     new CountryPM(1, "Deutschland", 357_121.41),
		                     new CountryPM(2, "Frankreich",  668_763.00),
		                     new CountryPM(3, "Italien",     301_338.00),
		                     new CountryPM(4, "Oesterreich",  83_878.99));
	}

	public ObservableList<CountryPM> getAllCountries() {
		return allCountries;
	}

	public String getApplicationTitle() {
		return applicationTitle.get();
	}

	public StringProperty applicationTitleProperty() {
		return applicationTitle;
	}

	public void setApplicationTitle(String applicationTitle) {
		this.applicationTitle.set(applicationTitle);
	}

}
