package ch.fhnw.richards.lecture09_JavaFX_lists_tables.nationalAssemblyElection;

public class NA_Controller {
	private NA_Model model;
	private NA_View view;
	
	public NA_Controller(NA_Model model, NA_View view) {
		this.model = model;
		this.view = view;
	}
}
