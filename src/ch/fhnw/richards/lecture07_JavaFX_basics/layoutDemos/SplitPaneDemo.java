package ch.fhnw.richards.lecture07_JavaFX_basics.layoutDemos;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.SplitPane;
import javafx.stage.Stage;

public class SplitPaneDemo extends Application {

	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		SplitPane root = new SplitPane();
		MyLabel lbl1 = new MyLabel("1");
		MyLabel lbl2 = new MyLabel("2");
		MyLabel lbl3 = new MyLabel("3");
		root.getItems().addAll(lbl1, lbl2, lbl3);

		// Create the scene using our layout; then display it
		Scene scene = new Scene(root);
		scene.getStylesheets().add(
				getClass().getResource("layouts.css").toExternalForm());
		primaryStage.setTitle("SplitPane Demo");
		primaryStage.setScene(scene);
		primaryStage.show();
	}
}
