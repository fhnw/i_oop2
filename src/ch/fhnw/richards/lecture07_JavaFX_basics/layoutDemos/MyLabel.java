package ch.fhnw.richards.lecture07_JavaFX_basics.layoutDemos;

import javafx.scene.control.Label;

/**
 * A label that takes up all available space
 */
public class MyLabel extends Label {
	public MyLabel(String text) {
		super(text);
		this.setMaxWidth(Double.MAX_VALUE);
		this.setMaxHeight(Double.MAX_VALUE);
	}
}
