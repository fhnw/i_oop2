package ch.fhnw.richards.lecture07_JavaFX_basics.layoutDemos;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class StackPaneDemo extends Application {

	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		StackPane root = new StackPane();
		
		Ellipse ell = new Ellipse(150, 80);
		ell.setFill(Color.LIGHTSALMON);
		
		Rectangle rect = new Rectangle(200, 100, Color.LIGHTBLUE);
		
		MyLabel lbl1 = new MyLabel("I am a label");
		
		root.getChildren().addAll(ell, rect, lbl1);

		// Create the scene using our layout; then display it
		Scene scene = new Scene(root);
		scene.getStylesheets().add(
				getClass().getResource("layouts.css").toExternalForm());
		primaryStage.setTitle("StackPane Demo");
		primaryStage.setScene(scene);
		primaryStage.show();
	}

}
