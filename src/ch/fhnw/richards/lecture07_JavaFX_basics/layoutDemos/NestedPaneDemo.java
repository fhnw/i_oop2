package ch.fhnw.richards.lecture07_JavaFX_basics.layoutDemos;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class NestedPaneDemo extends Application {

	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		BorderPane root = new BorderPane();

		VBox leftBox = new VBox(); root.setLeft(leftBox);
		VBox rightBox = new VBox(); root.setRight(rightBox);
		HBox topBox = new HBox(); root.setTop(topBox);
		HBox bottomBox = new HBox(); root.setBottom(bottomBox);
		
		for (int i = 1; i <= 5; i++) {
			leftBox.getChildren().add(new MyLabel("Left " + i));
			rightBox.getChildren().add(new MyLabel("Right " + i));
			topBox.getChildren().add(new MyLabel("Top " + i));
			bottomBox.getChildren().add(new MyLabel("Bottom " + i));
		}

		root.setCenter(new MyLabel("Center"));

		// Create the scene using our layout; then display it
		Scene scene = new Scene(root);
		scene.getStylesheets().add(
				getClass().getResource("layouts.css").toExternalForm());
		primaryStage.setTitle("BorderPane Demo");
		primaryStage.setScene(scene);
		primaryStage.show();
	}

}
