package ch.fhnw.richards.lecture07_JavaFX_basics.layoutDemos;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class BorderPaneDemo extends Application {

	public static void main(String[] args) {
		launch();
	}
	
	@Override
	public void start(Stage primaryStage) {
		BorderPane root = new BorderPane();
		
		MyLabel lblTop = new MyLabel("Top");
		root.setTop(lblTop);
		MyLabel lblBottom= new MyLabel("Bottom");
		root.setBottom(lblBottom);
		MyLabel lblLeft = new MyLabel("Left");
		root.setLeft(lblLeft);
		MyLabel lblRight = new MyLabel("Right");
		root.setRight(lblRight);
		MyLabel lblCenter = new MyLabel("Center");
		root.setCenter(lblCenter);

		// Create the scene using our layout; then display it
		Scene scene = new Scene(root);
		scene.getStylesheets().add(
				getClass().getResource("layouts.css").toExternalForm());
		primaryStage.setTitle("BorderPane Demo");
		primaryStage.setScene(scene);
		primaryStage.show();
	}

}
