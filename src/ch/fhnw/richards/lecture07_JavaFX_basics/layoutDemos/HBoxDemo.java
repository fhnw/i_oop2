package ch.fhnw.richards.lecture07_JavaFX_basics.layoutDemos;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class HBoxDemo extends Application {

	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		HBox root = new HBox();
		
		MyLabel lbl1 = new MyLabel("1");
		MyLabel lbl2 = new MyLabel("2");
		MyLabel lbl3 = new MyLabel("3");
		MyLabel lbl4 = new MyLabel("4");
		MyLabel lbl5 = new MyLabel("5");
		root.getChildren().addAll(lbl1, lbl2, lbl3, lbl4, lbl5);

		// Create the scene using our layout; then display it
		Scene scene = new Scene(root);
		scene.getStylesheets().add(
				getClass().getResource("layouts.css").toExternalForm());
		primaryStage.setTitle("HBox Demo");
		primaryStage.setScene(scene);
		primaryStage.show();
	}

}
