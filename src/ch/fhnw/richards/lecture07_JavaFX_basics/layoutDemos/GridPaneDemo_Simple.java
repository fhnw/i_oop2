package ch.fhnw.richards.lecture07_JavaFX_basics.layoutDemos;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class GridPaneDemo_Simple extends Application {

	public static void main(String[] args) {
		launch();
	}
	
	@Override
	public void start(Stage primaryStage) {
		GridPane root = new GridPane();
		
		for (int row = 0; row < 3; row++) {
			for (int col = 0; col < 5; col++) {
				MyLabel lblTmp = new MyLabel(row + ", " + col);
				root.add(lblTmp, col,  row);
			}
		}

		// Create the scene using our layout; then display it
		Scene scene = new Scene(root);
		scene.getStylesheets().add(
				getClass().getResource("layouts.css").toExternalForm());
		primaryStage.setTitle("GridPane Demo");
		primaryStage.setScene(scene);
		primaryStage.show();
	}

}
