package ch.fhnw.richards.lecture07_JavaFX_basics.layoutDemos;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.stage.Stage;

public class GridPaneDemo_Resizing extends Application {

	public static void main(String[] args) {
		launch();
	}
	
	@Override
	public void start(Stage primaryStage) {
		GridPane root = new GridPane();
		
		for (int row = 0; row < 3; row++) {
			for (int col = 0; col < 5; col++) {
				MyLabel lblTmp = new MyLabel(row + ", " + col);
				root.add(lblTmp, col,  row);
			}
		}
		
		// Set all columns to 20% of width
		for (int col = 0; col < 5; col++) {
			ColumnConstraints cc = new ColumnConstraints();
			cc.setPercentWidth(20);
			root.getColumnConstraints().add(cc);
		}
		
		// Set all rows to 33% of height
		for (int row = 0; row < 3; row++) {
			RowConstraints rc = new RowConstraints();
			rc.setPercentHeight(33);
			root.getRowConstraints().add(rc);
		}

		// Create the scene using our layout; then display it
		Scene scene = new Scene(root);
		scene.getStylesheets().add(
				getClass().getResource("layouts.css").toExternalForm());
		primaryStage.setTitle("GridPane Demo with Resizing");
		primaryStage.setScene(scene);
		primaryStage.show();
	}

}
