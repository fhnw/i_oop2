package ch.fhnw.richards.lecture07_JavaFX_basics.buttonClickDemo;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class ButtonClick5 extends Application {
	private int value; // our business data

	Label lblNumber = new Label(Integer.toString(value));
	Button btnClick = new Button("Click Me!");

	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage stage) throws Exception {
		GridPane root = new GridPane();
		root.add(lblNumber, 0, 0);
		root.add(btnClick, 0, 1);

		// register ourselves to listen for button clicks
		btnClick.setOnAction(this::doClick);

		Scene scene = new Scene(root);
		scene.getStylesheets().add(getClass().getResource("ButtonClick.css").toExternalForm());
		stage.setScene(scene);
		stage.setTitle("Button Click MVC");
		stage.show();
	}
	
	private void doClick(ActionEvent e) {
		value++; // business logic
		String newText = Integer.toString(value);
		lblNumber.setText(newText);
	}
}
