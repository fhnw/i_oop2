package ch.fhnw.richards.lecture07_JavaFX_basics.controlDemos;

import java.net.URL;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.FlowPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

// Copyright: http://www.java2s.com/Code/Java/JavaFX/Playmp3file.htm

public class AudioPlayer extends Application {

	MediaPlayer mediaPlayer;
	Media media;
	
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		FlowPane root = new FlowPane();
		// Copyright Song:
		// https://mp3truck.eu/hey-baby-dj-%C3%96tzi-mp3-download.html
		URL resource = getClass().getResource("dj_oetzi.wav");
		media = new Media(resource.toString());
		mediaPlayer = new MediaPlayer(media);
		mediaPlayer.play();
		Scene scene = new Scene(root, 200, 200, Color.BEIGE);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Audio Player 1");
		primaryStage.show();
	}
}
