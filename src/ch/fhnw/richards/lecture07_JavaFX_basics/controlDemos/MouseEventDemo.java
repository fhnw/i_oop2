package ch.fhnw.richards.lecture07_JavaFX_basics.controlDemos;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

// Copyright: http://stackoverflow.com/a/13360324

public class MouseEventDemo extends Application {

	public static void main(String[] args) {

		// Start the JavaFX application by calling launch().
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		BorderPane borderPane = new BorderPane();
		Rectangle rect = new Rectangle(200, 200);
		rect.setFill(Color.BLUE);
		borderPane.setCenter(rect);

		Scene scene = new Scene(borderPane, 400, 400);
		primaryStage.setTitle("Playing with Colors");
		primaryStage.setScene(scene);
		primaryStage.show();

		rect.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent t) {
				rect.setFill(Color.RED);
			}
		});
	}
}
