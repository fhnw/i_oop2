package ch.fhnw.richards.lecture07_JavaFX_basics.controlDemos;

// Load and display an image.
// Copyright: H. Schildt: Java: The Complete Reference (Complete Reference Series), Mcgraw-Hill Osborne Media, 9 edition 2014

import javafx.application.*;
import javafx.scene.*;
import javafx.stage.*;
import javafx.scene.layout.*;
import javafx.geometry.*;
import javafx.scene.image.*;

public class ImageDemo extends Application {

	public static void main(String[] args) {

		// Start the JavaFX application by calling launch().
		launch(args);
	}

	// Override the start() method.
	public void start(Stage myStage) {

		// Give the stage a title.
		myStage.setTitle("Display an Image");

		// Use a FlowPane for the root node.
		FlowPane rootNode = new FlowPane();

		// Use center alignment.
		rootNode.setAlignment(Pos.CENTER);

		// Create a scene.
		Scene myScene = new Scene(rootNode, 450, 450);

		// Set the scene on the stage.
		myStage.setScene(myScene);

		// Create an image view that uses the image.
		final ImageView imv = new ImageView();
		final Image image = new Image(
				ImageDemo.class.getResourceAsStream("hourglass.png"));
		imv.setImage(image);

		// Add the image view to the scene graph.
		rootNode.getChildren().add(imv);

		// Show the stage and its scene.
		myStage.show();
	}
}