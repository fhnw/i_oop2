package ch.fhnw.richards.lecture07_JavaFX_basics.controlDemos;

// Copyright: http://michaelkipp.de/processing/24%20gui.html

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class OkCancelButtons extends Application {
	Stage stage;

	@Override
	public void start(Stage stage) {
		this.stage = stage;
		
		Label label = new Label("Enter your password:");
		PasswordField textField = new PasswordField();

		HBox entryPane = new HBox(label, textField);
		entryPane.setAlignment(Pos.CENTER);
		entryPane.setPadding(new Insets(15));
		entryPane.setSpacing(10);

		Button bOk = new Button("OK");
		Button bCancel = new Button("Cancel");
		Label answerLabel = new Label("");
		HBox buttonPane = new HBox(answerLabel, bOk, bCancel);
		buttonPane.setAlignment(Pos.BASELINE_RIGHT);
		buttonPane.setPadding(new Insets(10));
		buttonPane.setSpacing(10);

		BorderPane pane = new BorderPane();

		pane.setCenter(entryPane);
		pane.setBottom(buttonPane);

		Scene scene = new Scene(pane, 350, 130);

		stage.setTitle("BorderPane");
		stage.setScene(scene);
		stage.show();

		// // Event Handler for Ok Button
		bOk.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				if (textField.getText().equals("admin")) {
					answerLabel.setText("Password ok");
				} else
					answerLabel.setText("Password NOT ok");
			}
		});

		// Event Handler for Cancel Button
		bCancel.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				stage.close();
			}
		});

	}

	public static void main(String[] args) {
		launch(args);
	}

}
