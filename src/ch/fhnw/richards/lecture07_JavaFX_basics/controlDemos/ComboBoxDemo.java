package ch.fhnw.richards.lecture07_JavaFX_basics.controlDemos;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class ComboBoxDemo extends Application {

	public static void main(String[] args) {

		// Start the JavaFX application by calling launch().
		launch(args);
	}

	public void start(Stage stage) {

		Label label = new Label("Choose Nationality");
		label.setFont(new Font(20));

		ComboBox<String> comboBox = new ComboBox<String>();
		comboBox.getItems().addAll("German", "American", "French");
		comboBox.setValue("German");

		// Layout
		BorderPane pane = new BorderPane(comboBox);
		Button ok = new Button("OK");
		pane.setTop(label);
		pane.setBottom(ok);
		pane.setPadding(new Insets(30));

		Scene scene = new Scene(pane);
		stage.setTitle("Where from");
		stage.setScene(scene);
		stage.show();
	}

}
