package ch.fhnw.richards.lecture07_JavaFX_basics.controlDemos;

import javafx.application.*;
import javafx.scene.*;
import javafx.stage.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.event.*;
import javafx.geometry.*;

public class BuyingGUI extends Application {

	CheckBox milk;
	CheckBox bread;
	CheckBox butter;

	Label response;
	Label allTargets;
	Label result; // Label for the final Price

	String targets = "";

	int price = 0;	// Final price

	public static void main(String[] args) {

		// Start the JavaFX application by calling launch().
		launch(args);
	}

	// Override the start() method.
	public void start(Stage myStage) {

		// Give the stage a title.
		myStage.setTitle("Our Sales");

		// Use a FlowPane for the root node. In this case,
		// vertical and horizontal gaps of 10.
		FlowPane rootNode = new FlowPane(10, 10);

		// Center the controls in the scene.
		rootNode.setAlignment(Pos.CENTER);

		// Create a scene.
		Scene myScene = new Scene(rootNode, 450, 200);

		// Set the scene on the stage.
		myStage.setScene(myScene);

		Label heading = new Label("Select Sales Options");

		// Create a label that will report the state of the
		// selected check box.
		response = new Label("No Sales Selected");

		// Create a label that will report all targets selected.
		allTargets = new Label("Sales List: <none>");

		// Create the check boxes.
		milk = new CheckBox("Milk (5)");
		bread = new CheckBox("Bread (10)");
		butter = new CheckBox("Butter (8)");

		// Create the button
		Button buy = new Button("Buy");

		// Create a label for the final price
		result = new Label("");

		// Handle action events for the check boxes and the button
		milk.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent ae) {
				if (milk.isSelected())
					response.setText("Milk selected.");
				else
					response.setText("Milk cleared.");

				showAll();
			}
		});

		bread.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent ae) {
				if (bread.isSelected())
					response.setText("Bread selected.");
				else
					response.setText("Bread cleared.");

				showAll();
			}
		});

		butter.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent ae) {
				if (butter.isSelected())
					response.setText("Butter selected.");
				else
					response.setText("Butter cleared.");

				showAll();
			}
		});

		buy.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				result.setText("Final price: " + price);

			}
		});

		// Add controls to the scene graph.
		rootNode.getChildren().addAll(heading, milk, bread, butter, response,
				allTargets, buy, result);

		// Show the stage and its scene.
		myStage.show();
	}

	// Update and show the targets list.
	void showAll() {
		targets = "";
		price = 0; // Avoids illegal cumulation
		if (milk.isSelected()) {
			targets = "Milk ";
			price += 5;
		}

		if (bread.isSelected()) {
			targets += "Bread ";
			price += 10;
		}

		if (butter.isSelected()) {
			targets += "Butter";
			price += 8;
		}

		if (targets.equals(""))
			targets = "<none>";

		allTargets.setText("Target List: " + targets);
	}
}
