package ch.fhnw.richards.lecture07_JavaFX_basics.controlDemos;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

// Copyright: http://stackoverflow.com/a/18761332

public class BrowserWindow extends Application {
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) {
		WebView webview = new WebView();
		webview.getEngine().load("http://google.ch");
		webview.setPrefSize(640, 390);
		stage.setScene(new Scene(webview));
		stage.show();
	}
}