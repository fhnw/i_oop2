package ch.fhnw.richards.lecture07_JavaFX_basics.solutionName;

public class NameModel {
	/**
	 * The only business-logic is a method to calculate
	 * the sum of the value of the letters in a name
	 * 
	 * @param name The input name
	 * @return the sum of the letter values
	 */
	public int calculateValue(String name) {
		name = name.toUpperCase();
		char[] chars = name.toCharArray();
		int sum = 0;
		System.out.println(name);
		for (char c : chars) {
			System.out.println(c);
			if (c >= 'A' && c <= 'Z') {
				sum += c + 1 - 'A';
			}					
		}
		return sum;
	}
}
