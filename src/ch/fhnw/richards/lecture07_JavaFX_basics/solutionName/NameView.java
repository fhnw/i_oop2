package ch.fhnw.richards.lecture07_JavaFX_basics.solutionName;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class NameView {
	private NameModel model;
	private Stage stage;

	protected Label lblName = new Label("Enter your name");;
	protected TextField txtName = new TextField();
	protected Button btnCalculate = new Button("Calculate value");
	protected Label lblValue = new Label();

	protected NameView(Stage stage, NameModel model) {
		this.stage = stage;
		this.model = model;

		stage.setTitle("Name calculator");

		GridPane root = new GridPane();
		root.add(lblName, 0, 0);
		root.add(txtName, 1, 0);
		root.add(btnCalculate, 0, 1, 2, 1);
		root.add(lblValue, 0, 2, 2, 1);
		
		lblValue.setId("value");

		Scene scene = new Scene(root);
		scene.getStylesheets().add(getClass().getResource("NameMVC.css").toExternalForm());
		stage.setScene(scene);
	}

	public void start() {
		stage.show();
	}

	/**
	 * Getter for the stage, so that the controller can access window events
	 */
	public Stage getStage() {
		return stage;
	}
}