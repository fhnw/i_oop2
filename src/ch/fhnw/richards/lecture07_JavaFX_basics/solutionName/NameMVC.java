package ch.fhnw.richards.lecture07_JavaFX_basics.solutionName;

import javafx.application.Application;
import javafx.stage.Stage;

public class NameMVC extends Application {
	private NameView view;
	private NameController controller;
	private NameModel model;

	public static void main(String[] args) {
		launch(args);
	}

	/**
	 * Note the dependencies between model, view and controller. Additionally,
	 * the view needs access to the primaryStage
	 */
	@Override
	public void start(Stage primaryStage) {
		// Initialize the GUI
		model = new NameModel();
		view = new NameView(primaryStage, model);
		controller = new NameController(model, view);

		// Display the GUI after all initialization is complete
		view.start();
	}
}
