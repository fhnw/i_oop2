package ch.fhnw.richards.lecture07_JavaFX_basics.solutionName;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.WindowEvent;

public class NameController {

	final private NameModel model;
	final private NameView view;

	protected NameController(NameModel model, NameView view) {
		this.model = model;
		this.view = view;

		// register ourselves to listen for button clicks
		view.btnCalculate.setOnAction((event) -> {
			int value = model.calculateValue(view.txtName.getText());
			view.lblValue.setText("The value is " + value);
		});
	}
}
