package ch.fhnw.richards.lecture08_JavaFX_advanced.simpleDrawing;

import java.util.ArrayList;

import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Shape;

/**
 * The model in a drawing program stores and manages image information. The view
 * can ask the model what needs drawn on the screen.
 * 
 * The API for the view is a list of objects that have been drawn. The view can
 * ask for objects out of this list.
 * 
 * The API for the controller are different drawing actions (ovals, lines, etc.)
 * that are stored in the list.
 */
public class SimpleDrawModel {
	// List of all objects drawn, newest first
	private ArrayList<Shape> drawingObjects = new ArrayList<>();

	//--- View API ---
	/**
	 * Return the newest object added to the drawing
	 */
	public Shape getLastObject() {
		return drawingObjects.isEmpty() ? null : drawingObjects.get(0);
	}
	
	//--- Controller API ---
	/**
	 * Draw a black dot
	 */
	public void drawPoint(Color c, double x, double y) {
		Shape s = new Ellipse(x, y, 0.5, 0.5);
		s.setStroke(c);
		s.setStrokeWidth(1);
		drawingObjects.add(0, s);
	}

	/**
	 * A circle is also an ellipse, and that's how we store it.
	 */
	public void drawCircle(Color c, double x, double y, double radius) {
		Shape s = new Ellipse(x, y, radius, radius);
		s.setStroke(c);
		s.setStrokeWidth(1);
		drawingObjects.add(0, s);
	}

	/**
	 * Add a a new ellipse to the drawing (center_x, center_y, radius_x, radius_y)
	 */
	public void drawEllipse(Color c, double x, double y, double radius_x, double radius_y) {
		Shape s = new Ellipse(x, y, radius_x, radius_y);
		s.setStroke(c);
		s.setStrokeWidth(1);
		drawingObjects.add(0, s);
	}
	
	/**
	 * Erase all objects
	 */
	public void erase() {
		drawingObjects = new ArrayList<>();
	}
}
