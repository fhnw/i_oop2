package ch.fhnw.richards.lecture08_JavaFX_advanced.simpleDrawing;

import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Shape;
import javafx.stage.Stage;

public class SimpleDrawView {
	private SimpleDrawModel model;

	// Important GUI elements - accessible to the controller
	protected Stage stage;
	protected MenuItem miFollowMouse = new MenuItem("Follow mouse");
	protected MenuItem miDrawCircle = new MenuItem("DrawCircle");
	protected MenuItem miErase = new MenuItem("Erase drawing");
	protected Canvas canvas = new Canvas(400, 400);

	// GraphicsContext for drawing on the canvas
	private GraphicsContext gc = canvas.getGraphicsContext2D();

	public SimpleDrawView(Stage primaryStage, SimpleDrawModel model) {
		this.stage = primaryStage;
		this.model = model;

		// Set up the menus
		Menu drawMenu = new Menu("Draw");
		drawMenu.getItems().addAll(miFollowMouse, miDrawCircle, miErase);
		Menu helpMenu = new Menu("Help");
		MenuBar menuBar = new MenuBar();
		menuBar.getMenus().addAll(drawMenu, helpMenu);

		// Put together the layout
		BorderPane root = new BorderPane();
		root.setTop(menuBar);
		root.setCenter(canvas);

		// Erase the canvas to white
		erase();

		// Last steps
		Scene scene = new Scene(root);
		stage.setTitle("Simple Drawing");
		stage.setScene(scene);
		;
	}

	public void start() {
		stage.show();
	}

	/**
	 * Stopping the view - just make it invisible
	 */
	public void stop() {
		stage.hide();
	}

	// --- API ---

	/**
	 * Erase the canvas contents to white
	 */
	public void erase() {
		gc.setFill(Color.WHITE);
		gc.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
	}

	/**
	 * Get the newest shape from the model, and draw it on the canvas.
	 * 
	 * Note: JavaFX is not consistent. Ellipses are stored with center/radius,
	 * but they are drawn using the top-left corner, the width and the height.
	 */
	public void drawNewest() {
		Shape shape = model.getLastObject();
		gc.setStroke(shape.getStroke());

		if (shape instanceof Ellipse) {
			Ellipse ellipse = (Ellipse) shape;
			double left = ellipse.getCenterX() - ellipse.getRadiusX();
			double top = ellipse.getCenterY() - ellipse.getRadiusY();
			double width = ellipse.getRadiusX() * 2;
			double height = ellipse.getRadiusY() * 2;

			gc.strokeOval(left, top, width, height);
		} else if (shape instanceof Ellipse) {

		}
	}
}
