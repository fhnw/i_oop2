package ch.fhnw.richards.lecture08_JavaFX_advanced.simpleDrawing;

import javafx.event.Event;
import javafx.geometry.Point2D;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

public class SimpleDrawController {
	private SimpleDrawModel model;
	private SimpleDrawView view;

	private enum DrawingMode {
		FollowMouseMode, CircleMode
	};
	private DrawingMode drawingMode = DrawingMode.FollowMouseMode;

	// Used in DrawCircle mode, to remember the start of the circle
	private Point2D pressedPoint;
	
	protected SimpleDrawController(SimpleDrawModel model, SimpleDrawView view) {
		this.model = model;
		this.view = view;

		// register ourselves to handle window-closing event
		view.stage.setOnCloseRequest(event -> view.stop());

		// register ourselves for the menu items
		view.miDrawCircle.setOnAction(event -> doMenu(event));
		view.miFollowMouse.setOnAction(event -> doMenu(event));
		view.miErase.setOnAction(event -> doMenu(event));

		// register ourselves for mouse events on the canvas
		view.canvas.setOnMousePressed(event -> doMouse(event));
		view.canvas.setOnMouseReleased(event -> doMouse(event));
		view.canvas.setOnMouseDragged(event -> doMouse(event));
	}

	/**
	 * Process menu events
	 */
	private void doMenu(Event event) {
		if (event.getSource() == view.miDrawCircle) {
			drawingMode = DrawingMode.CircleMode;
		} else if (event.getSource() == view.miFollowMouse) {
			drawingMode = DrawingMode.FollowMouseMode;
		} else if (event.getSource() == view.miErase) {
			model.erase();
			view.erase();
		}
	}

	/**
	 * Process mouse events.
	 * 
	 * - In FollowMouseMode, every mouse event results in a new object to draw.
	 * 
	 * - In CircleMode, only MouseRelease creates a new object.
	 */
	private void doMouse(Event event) {
		MouseEvent me = (MouseEvent) event;
		double x = me.getX();
		double y = me.getY();

		if (drawingMode == DrawingMode.FollowMouseMode) {
			if (me.getEventType() == MouseEvent.MOUSE_PRESSED) {
				model.drawCircle(Color.GREEN, x, y, 3);
			} else if (me.getEventType() == MouseEvent.MOUSE_RELEASED) {
				model.drawCircle(Color.RED, x, y, 3);
			} else if (me.getEventType() == MouseEvent.MOUSE_DRAGGED) {
				model.drawPoint(Color.BLACK, x, y);
			}
			view.drawNewest();
		} else { // CircleMode
			if (me.getEventType() == MouseEvent.MOUSE_PRESSED) {
				pressedPoint = new Point2D(x, y);
			} else if (me.getEventType() == MouseEvent.MOUSE_RELEASED) {
				double cx = (x + pressedPoint.getX()) / 2;
				double rx = Math.abs(x - cx);
				double cy = (y + pressedPoint.getY()) / 2;
				double ry = Math.abs(y - cy);
				model.drawEllipse(Color.BLUE, cx, cy, rx, ry);
				view.drawNewest();
			}			
		}
	}
}
