package ch.fhnw.richards.lecture08_JavaFX_advanced;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class WindowEventExample extends Application {
	private int tries = 0;

	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Label lblMessage = new Label("You have to try three\ntimes to close this window!");
		
		primaryStage.setOnCloseRequest( event -> {
			// Consume the first two events
			if (++tries < 3) event.consume();
		} );
		
		Scene scene = new Scene(lblMessage);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	

}
