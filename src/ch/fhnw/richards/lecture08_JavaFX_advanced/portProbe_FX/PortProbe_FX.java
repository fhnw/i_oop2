package ch.fhnw.richards.lecture08_JavaFX_advanced.portProbe_FX;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class PortProbe_FX extends Application {
	private PortProbeModel model;
	private PortProbeView view;
	private PortProbeController controller;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		model = new PortProbeModel();
		view = new PortProbeView(primaryStage, model);
		controller = new PortProbeController(model, view);
		view.start();
	}

	public PortProbeModel getProbeLogic() {
		return model;
	}
	
}
