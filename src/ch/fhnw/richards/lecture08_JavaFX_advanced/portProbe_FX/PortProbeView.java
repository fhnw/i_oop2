package ch.fhnw.richards.lecture08_JavaFX_advanced.portProbe_FX;

import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class PortProbeView {
	private Stage primaryStage;
	private PortProbeModel model;
	TopControlPane topControlPane;

	Label lblResult = new Label();

	public PortProbeView(Stage primaryStage, PortProbeModel model) {
		this.primaryStage = primaryStage;
		this.model = model;

		BorderPane root = new BorderPane();
		lblResult.setStyle("-fx-font-size: 30px");
		root.setCenter(lblResult);

		// Validate each control individually when focus is lost
		this.topControlPane =new TopControlPane(this, model); 
		root.setTop(topControlPane);

		// Create the scene using our layout; then display it
		Scene scene = new Scene(root);
		primaryStage.setTitle("Port Probe");
		primaryStage.setScene(scene);
	}

	public void setProbeResult(boolean success) {
		if (success) {
			lblResult.setText("Port is open");
		} else {
			lblResult.setText("Port is closed or stealthed");
		}
	}

	public void clearProbeResult() {
		lblResult.setText("");
	}

	void start() {
		primaryStage.show();
	}
}
