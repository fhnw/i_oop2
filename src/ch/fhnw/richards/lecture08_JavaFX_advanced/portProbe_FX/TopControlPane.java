package ch.fhnw.richards.lecture08_JavaFX_advanced.portProbe_FX;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;

/**
 * This class implements the top row of controls, where the IP address and port
 * number are entered. It uses focus-based validation, checking the data when
 * the user tries to leave a data-entry field.
 */
public class TopControlPane extends HBox {
	// Controls
	Label lblIP = new Label("IP address");
	TextField txtIP = new TextField("127.0.0.1");
	Region spacer = new Region();
	Label lblPort = new Label("Port number");
	TextField txtPort = new TextField("80");
	Region spacer2 = new Region();
	Button btnOk = new Button("OK");
	

	public TopControlPane(PortProbeView main, PortProbeModel model) {
		super(20);
		
		txtIP.setPrefSize(150, 30);
		txtIP.setStyle("-fx-background-color:rgba(192, 255, 192, 1.0)");
		txtPort.setPrefSize(100, 30);
		txtPort.setStyle("-fx-background-color:rgba(192, 255, 192, 1.0)");
		spacer.setPrefSize(30, 30);
		spacer2.setPrefSize(30, 30);

		this.getChildren().add(lblIP);
		this.getChildren().add(txtIP);
		this.getChildren().add(spacer);
		this.getChildren().add(lblPort);
		this.getChildren().add(txtPort);
		this.getChildren().add(spacer2);
		this.getChildren().add(btnOk);
	}
}
