package ch.fhnw.richards.lecture08_JavaFX_advanced.portProbe_FX;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import javafx.scene.control.TextField;

public class TopControlPaneController {
	
	// Values from our controls, after validation
	private String ipAddress;
	private int portNumber;
	
	TopControlPaneController(PortProbeModel model, PortProbeView view) {
		
		// Shorter references to the controls
		TextField txtIP = view.topControlPane.txtIP;
		TextField txtPort = view.topControlPane.txtPort;
		Button btnOk = view.topControlPane.btnOk;
		
		view.topControlPane.btnOk.setOnAction(e -> {
			boolean success = model.probe(ipAddress, portNumber);
			view.setProbeResult(success);
		});

		// Focus handling - validate on lostFocus;
		// reclaim focus in case of error
		txtIP.focusedProperty().addListener((control, lostFocus, hasFocus) -> {
			if (lostFocus) {
				ipAddress = txtIP.getText();
				boolean valid = isValidWebAddress(ipAddress);
				setGreenRed(txtIP, valid);
				if (!valid) txtIP.requestFocus();
			}
		});
		
		txtPort.focusedProperty().addListener((control, lostFocus, hasFocus) -> {
			if (lostFocus) {
				boolean valid = isValidPortNumber(txtPort.getText());
				setGreenRed(txtPort, valid);
				if (valid) {
					portNumber = Integer.parseInt(txtPort.getText());
				} else {
					txtPort.requestFocus();
				}
			}
		});
		btnOk.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean lostFocus, Boolean hasFocus) {
				if (lostFocus) {
					view.clearProbeResult();
				}
			}
		});		
	}
	
	private boolean isValidWebAddress(String newValue) {
		boolean valid = false;
		String[] parts = newValue.split("\\.", -1);

		// check for a numeric address first: 4 parts, each an integer 0 to 255
		if (parts.length == 4) {
			valid = true;
			for (String part : parts) {
				try {
					int value = Integer.parseInt(part);
					if (value < 0 || value > 255) valid = false;
				} catch (NumberFormatException e) {
					// wasn't an integer
					valid = false;
				}
			}
		}

		// If not valid, try for a symbolic address: at least two parts, each
		// part at least two characters. We don't bother checking what kinds of
		// characters they are.
		if (!valid) {
			if (parts.length >= 2) {
				valid = true;
				for (String part : parts) {
					if (part.length() < 2) valid = false;
				}
			}
		}

		return valid;
	}

	private boolean isValidPortNumber(String newValue) {
		boolean valid = true;
		try {
			int value = Integer.parseInt(newValue);
			if (value < 1 || value > 65535) valid = false;
		} catch (NumberFormatException e) {
			// wasn't an integer
			valid = false;
		}
		return valid;
	}
	
	private void setGreenRed(Control control, boolean result) {
		if (result) {
			control.setStyle("-fx-background-color:rgba(208, 255, 208, 1.0)");
		} else {
			control.setStyle("-fx-background-color:rgba(255, 208, 208, 1.0)");
		}
	}
}
