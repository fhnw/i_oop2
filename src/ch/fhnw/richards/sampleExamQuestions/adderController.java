package ch.fhnw.richards.sampleExamQuestions;

import javafx.application.Platform;

public class adderController {
	final private adderModel model;
	final private adderView view;
	
	protected adderController(adderModel model, adderView view) {
		this.model = model;
		this.view = view;
		
		// Register ourselves to listen for property changes in the model.
		// If the new value is below zero, we call view.doAnimate();
		model.getValueProperty().addListener( (observable, oldValue, newValue) -> {
			if (newValue.intValue() < 0) {
		        // Move to the JavaFX thread: Note, not necessary in this simple example
				// because model changes are only triggered by user interaction. In a
				// more complex application, this would be essential!
		        Platform.runLater(new Runnable() {
		            @Override public void run() {
						view.doAnimate();
		            }
		        });
			}
		});
		
		// register ourselves to listen for button clicks. When the button
        // is clicked, what whatever is needed for the application to work
		view.btnAdd.setOnAction((event) -> {
		        model.addToValue(Integer.parseInt(view.txtAmount.getText()));
			}
		);
	}
}
