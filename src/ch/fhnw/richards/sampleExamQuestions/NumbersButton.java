package ch.fhnw.richards.sampleExamQuestions;

import javafx.scene.control.Button;
import javafx.scene.text.Font;

/**
 * Really, we shouldn't set styles in code. This is just an excuse to have our
 * own class. In a real application, you might define special functionality,
 * validation, user feedback or something else.
 */
public class NumbersButton extends Button {
	protected NumbersButton(String text) {
		super(text);
		
		this.setFont(new Font("Arial Black", 12));
		this.setStyle("-fx-background-color: firebrick");
		this.setStyle("-fx-text-fill: white");
		this.setStyle("-fx-background-radius: 10px");
	}
}
