package ch.fhnw.richards.sampleExamQuestions;

import javafx.scene.control.Label;
import javafx.scene.text.Font;

/**
 * Really, we shouldn't set styles in code. This is just an excuse to have our
 * own class. In a real application, you might define special functionality,
 * validation, user feedback or something else.
 */
public class NumbersResultLabel extends Label {
	protected NumbersResultLabel(String text) {
		super(text);
		this.setMinWidth(150);
		this.setStyle("-fx-background-color: lightblue");
		this.setFont(new Font("Arial Black", 12));
	}
}
