package ch.fhnw.richards.sampleExamQuestions;

import javafx.application.Application;
import javafx.stage.Stage;

public class adderMain extends Application {
	private adderView view;
	private adderController controller;
	private adderModel model;

	public static void main(String[] args) {
		launch(args);
	}

	/**
	 * Note the dependencies between model, view and controller. Additionally,
	 * the view needs access to the primaryStage
	 */
	@Override
	public void start(Stage primaryStage) {
		// Initialize the GUI
		model = new adderModel();
		view = new adderView(primaryStage, model);
		controller = new adderController(model, view);

		// Display the GUI after all initialization is complete
		view.start();
	}
}
