package ch.fhnw.richards.sampleExamQuestions;

import javafx.scene.Scene;
import javafx.stage.Stage;

public class NumbersView {
	private NumbersModel model;
	private Stage stage;
	
	NumbersLayoutPane root;
	
	protected NumbersView(Stage stage, NumbersModel model) {
		this.stage = stage;
		this.model = model;

		// Create layout
		root = new NumbersLayoutPane();

		// Set up stage and scene
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.setTitle("Number converter");
	}
	
	/**
	 * Method to display the GUI, after all MVC initialization is complete
	 */
	public void start() {
		stage.show();
	}

	/**
	 * Getter for the stage, so that the controller can access window events
	 */
	protected Stage getStage() {
		return stage;
	}
}
