package ch.fhnw.richards.sampleExamQuestions;

import javafx.beans.property.SimpleIntegerProperty;

public class adderModel {
	private SimpleIntegerProperty value;
	
	protected adderModel() {
		value = new SimpleIntegerProperty(0);
	}
	
	// Business logic
	public void addToValue(int amount) {
		setValue(getValue() + amount);
	}
	
	// Getters and Setters
	
	public SimpleIntegerProperty getValueProperty() {
		return value;
	}
	
	public int getValue() {
		return value.get();
	}
	
	public void setValue(int newValue) {
		value.set(newValue);
	}
}
