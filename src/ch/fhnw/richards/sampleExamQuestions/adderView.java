package ch.fhnw.richards.sampleExamQuestions;

import javafx.animation.ScaleTransition;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;

public class adderView {
	private adderModel model;
	private Stage stage;

	// Set up controls
	protected Label lblTotal = new Label();
	protected Button btnAdd = new Button("Add amount to total");
	protected TextField txtAmount = new TextField("0");

	private ScaleTransition pulse;

	protected adderView(Stage stage, adderModel model) {
		this.stage = stage;
		this.model = model;

		// Bind lblTotal, so that it automatically updates from the model
		lblTotal.textProperty().bind(model.getValueProperty().asString());

		// Create layout
		Pane root = createLayout();

		// Prepare animation for use
		pulse = createAnimation();

		// Set up stage and scene
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.setTitle("Adder");
	}

	/**
	 * Method to set up the layout
	 */
	private Pane createLayout() {
		btnAdd.setMaxWidth(Integer.MAX_VALUE); // fill width of VBox
		Region spacer = new Region(); // Empty area that can grow with vertical resizing
		VBox root = new VBox(txtAmount, btnAdd, spacer, lblTotal);
		VBox.setVgrow(spacer, Priority.ALWAYS); // Give extra vertical space to spacer
		root.setPadding(new Insets(10, 10, 10, 10)); // space around edge of window
		root.setSpacing(10); // space between controls
		return root;
	}

	/**
	 * Method to set up the animation: The label should increase in size by 50%,
	 * and then decrease back to normal.
	 */
	private ScaleTransition createAnimation() {
		ScaleTransition st = new ScaleTransition(Duration.millis(1000), lblTotal);
		st.setByX(1.5f);
		st.setByY(1.5f);
		st.setCycleCount(2);
		st.setAutoReverse(true);
		return st;
	}

	/**
	 * Method to display the GUI, after all MVC initialization is complete
	 */
	public void start() {
		stage.show();
	}

	/**
	 * Getter for the stage, so that the controller can access window events
	 */
	protected Stage getStage() {
		return stage;
	}

	/**
	 * Method to allow the controller to trigger an animation
	 */
	protected void doAnimate() {
		pulse.play();
	}
}
