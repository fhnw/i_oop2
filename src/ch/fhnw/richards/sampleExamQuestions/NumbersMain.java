package ch.fhnw.richards.sampleExamQuestions;

import javafx.application.Application;
import javafx.stage.Stage;

public class NumbersMain extends Application {
	private NumbersView view;
	private NumbersController controller;
	private NumbersModel model;

	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// Initialize the GUI
		model = new NumbersModel();
		view = new NumbersView(primaryStage, model);
		controller = new NumbersController(model, view);

		// Display the GUI after all initialization is complete
		view.start();
	}
}
