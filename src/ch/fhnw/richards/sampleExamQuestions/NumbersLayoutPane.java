package ch.fhnw.richards.sampleExamQuestions;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

public class NumbersLayoutPane extends HBox {
	// Declare and initialize controls
	TextField txtStartNumber = new TextField();
	NumbersButton btnToHex = new NumbersButton("To Hex");
	NumbersButton btnToBinary = new NumbersButton("To Binary");
	NumbersResultLabel lblResult = new NumbersResultLabel("");

	protected NumbersLayoutPane() {
		super();
		this.setPadding(new Insets(10, 10, 10, 10)); // space around edge of window
		this.setSpacing(10); // space between controls
		this.setAlignment(Pos.CENTER); // center elements in the box
		
		// Box to hold the buttons		
		VBox buttonBox = new VBox(btnToHex, btnToBinary);
		buttonBox.setSpacing(10);
		
		// Two regions to grow horizontally
		Region spacer1 = new Region();
		Region spacer2 = new Region();
		
		this.getChildren().addAll(txtStartNumber, spacer1, buttonBox, spacer2, lblResult);
		
		// Regions get extra space
		HBox.setHgrow(spacer1, Priority.ALWAYS); // Give extra vertical space to spacer
		HBox.setHgrow(spacer2, Priority.ALWAYS); // Give extra vertical space to spacer		
	}
}
