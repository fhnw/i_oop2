package ch.fhnw.richards.sampleExamQuestions;

public class NumbersController {
	final private NumbersModel model;
	final private NumbersView view;

	protected NumbersController(NumbersModel model, NumbersView view) {
		this.model = model;
		this.view = view;

		// When the value of the input field changes, enable buttons
		// if it is a  valid decimal number
		view.root.txtStartNumber.textProperty().addListener((observable, oldvalue, newValue) -> {
			boolean isValid = false;
			try {
				Integer.parseInt(newValue);
				isValid = true;
			} catch (NumberFormatException e) {
				isValid = false;
			}
			view.root.btnToHex.setDisable(!isValid);
			view.root.btnToBinary.setDisable(!isValid);
		});

		// register ourselves to listen for button clicks. When a button
        // is clicked, what whatever is needed for the application to work
		view.root.btnToHex.setOnAction((event) -> {
			String str = model.toHex(view.root.txtStartNumber.getText());
			view.root.lblResult.setText(str);
		});
		view.root.btnToBinary.setOnAction((event) -> {
			String str = model.toBinary(view.root.txtStartNumber.getText());
			view.root.lblResult.setText(str);
		});
	}
}
