package ch.fhnw.richards.lecture06_sampleMidtermSolutions;

public class Clothing {
	private int size;
	private String name;
	private char group; // 'w' = woman 'm' = man 'k' = kid

	public Clothing(int size, String name, char group) {
	this.size = size;
	this.name = name;
	this.group = group;
	}

	public Clothing() { }

	@Override
	public String toString() {
		return "Clothing [" + size + ", " + name + ", " + group + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof Clothing) {
			Clothing k = (Clothing) o;
			if (k.getGroup() == this.getGroup() && k.getSize() == this.getSize()) {
				return true;
			}
		}
		return false;
	}

	// Getters and setters as usual
	
	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public char getGroup() {
		return group;
	}

	public void setGroup(char group) {
		this.group = group;
	}
}
