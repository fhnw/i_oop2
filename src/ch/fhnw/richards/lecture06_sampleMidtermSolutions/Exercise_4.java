package ch.fhnw.richards.lecture06_sampleMidtermSolutions;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Exercise_4 {

	public static <T> List<T> getOrder(int[] quantities, T[] items) {
		List<T> orderItems = new ArrayList<>();
		for (int i = 0; i < items.length; i++) {
			int qty = (i < quantities.length) ? quantities[i] : 0;
			for (int j = 0; j < qty; j++) {
				orderItems.add(items[i]);
			}
		}
		return orderItems;
	}
	
	// --- main method for example usage ---

	public static void main(String[] args) {
		Clothing[] items = new Clothing[5];
		items[0] = new Shirt("Blue");
		items[1] = new Sweater("Merino");
		items[2] = new Top("Anything");
		items[3] = new Shoe("Boot");
		items[4] = new WorkShoe("Rugged");
		int[] quantities = { 3, 1, 2, 4, 5 };
		List<Clothing> list = getOrder(quantities, items);
		System.out.println(list.size()); // 15 items

		List<Shoe> shoes = new ArrayList<>();
		// Assume a constructor that takes only the size as a parameter
		shoes.add(new Shoe(34));
		shoes.add(new WorkShoe(36));
		shoes.add(new WorkShoe(38));
		shoes.add(new Shoe(38));
		List<Shoe> size38 = getAllShoesWithSize(38, shoes);
		System.out.println(size38.size()); // 2 shoes
		List<WorkShoe> shoes2 = new ArrayList<>();
		shoes2.add(new WorkShoe(36));
		shoes2.add(new WorkShoe(38));
		shoes2.add(new WorkShoe(42));
		shoes2.add(new WorkShoe(38));
		List<WorkShoe> size42 = getAllShoesWithSize(42, shoes2);
		System.out.println(size42.size()); // 1 shoe
	}

	public static <T extends Shoe> List<T> getAllShoesWithSize(int size, List<T> shoes) {
		return shoes.stream().filter(s -> s.getSize() == size).collect(Collectors.toList());
	}

	// Class declarations
	public static class Top extends Clothing {
		public Top(String name) {
			super(10, name, 'k');
		}
	}

	public static class Shirt extends Top {
		public Shirt(String name) {
			super(name);
		}
	}

	public static class Sweater extends Top {
		public Sweater(String name) {
			super(name);
		}
	}

	public static class Shoe extends Clothing {
		public Shoe(String name) {
			super(10, name, 'k');
		}

		public Shoe(int size) {
			super(size, "unnamed", 'm');
		}
	}

	public static class WorkShoe extends Shoe {
		public WorkShoe(String name) {
			super(name);
		}

		public WorkShoe(int size) {
			super(size);
		}
	}
}
