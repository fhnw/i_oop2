package ch.fhnw.richards.lecture06_sampleMidtermSolutions;

import java.util.HashSet;
import java.util.Set;

public class Exercise_1_1 {

	public static void main(String[] args) {
		Clothing k1 = new Clothing(41, "Sweater", 'm');
		Clothing k2 = new Clothing(41, "Pants", 'm');
		Clothing k3 = new Clothing(56, "Shirt", 'k');
		Set<Clothing> set = new HashSet<>();
		set.add(k1);
		set.add(k2);
		set.add(k3);
		set.add(k2);
		set.add(k1);
		for (Clothing k : set) {
			System.out.println(k);
		}
	}
}
