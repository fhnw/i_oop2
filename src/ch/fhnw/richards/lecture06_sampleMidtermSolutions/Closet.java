package ch.fhnw.richards.lecture06_sampleMidtermSolutions;

import java.util.ArrayList;

public class Closet<T extends Clothing> {
		private ArrayList<T> items = new ArrayList<>();
		
		public void add(T item) {
			if (items.isEmpty() || items.get(0).getGroup() == item.getGroup())
				items.add(item);
		}
		
		public boolean allSameSize() {
			boolean result = true;
			if (!items.isEmpty()) {
				int size = items.get(0).getSize(); 
				result = items.stream().allMatch(c -> c.getSize() == size);				
			}
			return result;
		}

		public T getElement(String name) {
			return items.stream().filter(c -> c.getName().equals(name)).findFirst().get();
		}
}
