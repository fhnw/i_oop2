package ch.fhnw.richards.lecture06_sampleMidtermSolutions;

public class Exercise_3_2 {
	// Error: missing cast
	private static void snippet1() {
//		Closet<Top> closet = new Closet<>();
//		closet.add(new Sweater("Merino"));
//		Sweater s = closet.getElement("Merino");
	}

	// Ok
	private static void snippet2() {
//		Closet<Sweater> closet = new Closet<>();
//		closet.add(new Sweater("Merino"));
//		Clothing c = closet.getElement("Merino");
	}

	// Closet<Shoe> is not a superclass of Closet<WorkShoe>
	private static void snippet3() {
//		Closet<Shoe> closet = new Closet<WorkShoe>();
//		closet.add(new Shoe("Boot"));
//		Shoe s = closet.getElement("Boot");
	}

	// Cannot add a Shirt to a Closet<Sweater>
	private static void snippet4() {
//		Closet<Sweater> closet = new Closet<>();
//		closet.add(new Shirt("T-shirt"));
//		Object p = closet.getElement("T-shirt");
	}

	// ariable "closet" can hold different types of closets, but we
	// cannot directly add Sweaters to it, because Closet<Top> is not a
	// superclass of Closet<Sweater>
	private static void snippet5() {
//		Closet<? extends Top> closet = new Closet<Sweater>();
//		closet.add(new Sweater("Merino"));
//		Top t = closet.getElement("Merino");
	}

	// Class declarations
	public static class Top extends Clothing {
		public Top(String name) {
			super(10, name, 'k');
		}
	}

	public static class Shirt extends Top {
		public Shirt(String name) {
			super(name);
		}
	}

	public static class Sweater extends Top {
		public Sweater(String name) {
			super(name);
		}
	}

	public static class Shoe extends Clothing {
		public Shoe(String name) {
			super(10, name, 'k');
		}
	}

	public static class WorkShoe extends Shoe {
		public WorkShoe(String name) {
			super(name);
		}
	}
}
