package ch.fhnw.richards.lecture06_sampleMidtermSolutions;

import java.util.ArrayList;
import java.util.List;

public class Exercise_1_3 {
	public static void main(String[] args) {
		Clothing k1 = new Clothing(41, "Sweater", 'm');
		Clothing k2 = new Clothing(41, "Pants", 'm');
		Clothing k3 = new Clothing(56, "Shirt", 'k');
		List<Clothing> list = new ArrayList<>();
		list.add(k1);
		list.add(k2);
		list.add(k3);
		list.add(k2);
		list.add(k1);
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i).getName());
		}
	}
}
