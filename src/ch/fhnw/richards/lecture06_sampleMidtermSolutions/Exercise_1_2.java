package ch.fhnw.richards.lecture06_sampleMidtermSolutions;

import java.util.HashMap;
import java.util.Map;

public class Exercise_1_2 {

	public static void main(String[] args) {
		Clothing k1 = new Clothing(41, "Sweater", 'm');
		Clothing k2 = new Clothing(41, "Pants", 'm');
		Clothing k3 = new Clothing(56, "Shirt", 'k');
		Map<Clothing, String> map = new HashMap<>();
		map.put(k1, "Urs");
		map.put(k2, "Urs");
		map.put(k3, "Heidi");
		map.put(k2, "Peter");
		for (String s : map.values()) {
			System.out.println(s);
		}
	}
}
