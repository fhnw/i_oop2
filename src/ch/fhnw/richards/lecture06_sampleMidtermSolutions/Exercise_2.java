package ch.fhnw.richards.lecture06_sampleMidtermSolutions;

import java.util.Comparator;
import java.util.List;

public class Exercise_2 {

	public static void keepSameSize(int size, List<Clothing> list) {
		list.removeIf(c -> c.getSize() != size);
	}
	
	public static void replaceSize(int size, List<Clothing> list) {
		list.forEach(c -> c.setSize(size));
	}
	
	public static int maximumKidSize(List<Clothing> list) {
		return list.stream()
				.filter(c -> c.getGroup() == 'k')
				.max(Comparator.comparing(Clothing::getSize))
				.get()
				.getSize();
	}

}
